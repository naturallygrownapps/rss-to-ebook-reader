﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSStoReader
{
    public static class Extensions
    {
        public static string GetMessage(this Exception self)
        {
            if (self is AggregateException)
            {
                var a = (AggregateException)self;
                return a.Flatten().InnerExceptions.First().Message;
            }
            else
            {
                return self.Message;
            }
        }
    }
}
