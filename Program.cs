﻿using Argotic.Common;
using Argotic.Syndication;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Mail;
using System.Reflection;
using System.ServiceModel.Syndication;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace RSStoReader
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                Console.Error.WriteLine("Usage: " + Path.GetFileName(Assembly.GetExecutingAssembly().Location) + " <configfile>");
                args = new[] { "config.json" };
            }

            Console.WriteLine("Reading configuration.");
            var configFile = File.ReadAllText(args[0]);
            var config = JsonConvert.DeserializeObject<Config>(configFile);

            Console.WriteLine("Reading RSS Feeds.");
            var feeds = GetFeeds(config).Result;

            Console.WriteLine("Saving feeds to epub files.");
            var generatedFiles = Task.WhenAll(feeds.Select(async f =>
            {
                try
                {
                    return await ToEpub(f);
                }
                catch (Exception ex)
                {
                    Console.Error.WriteLine("Error writing rss epub file.\n" + ex.ToString());
                    return null;
                }
            })).Result.Where(f => !string.IsNullOrWhiteSpace(f));
            
            if (generatedFiles.Any())
            {
                Console.WriteLine("Sending mails to reader.");
                try
                {
                    SendMailForGeneratedFile(generatedFiles, config);
                }
                catch (Exception ex)
                {
                    Console.Error.WriteLine("Error sending mail.\n" + ex.ToString());
                }
            }
        }

        static async Task<IEnumerable<GenericSyndicationFeed>> GetFeeds(Config feedConfig)
        {
            var result = new List<GenericSyndicationFeed>();
            var client = new HttpClient();
            foreach(var feedUrl in feedConfig.Feeds)
            {
                var feedXmlStr = await client.GetStringAsync(feedUrl);
                var feed = new GenericSyndicationFeed();
                feed.Load(feedXmlStr);
                result.Add(feed);
            }
            return result;
        }

        static string EnsureValidFilename(string filename)
        {
            return new string(filename.Where(c => !Path.GetInvalidFileNameChars().Contains(c)).ToArray());
        }

        static string GetAuthorFromFeed(GenericSyndicationFeed feed)
        {
            var author = "";
            if (feed.Format == SyndicationContentFormat.Rss)
            {
                RssFeed rssFeed = feed.Resource as RssFeed;
                if (rssFeed != null)
                {
                    author = rssFeed.Channel.ManagingEditor;
                }
            }
            else if (feed.Format == SyndicationContentFormat.Atom)
            {
                AtomFeed atomFeed = feed.Resource as AtomFeed;
                if (atomFeed != null)
                {
                    author = atomFeed.Authors.FirstOrDefault()?.Name;
                }
            }

            return !string.IsNullOrWhiteSpace(author) ? author : "Unbekannt";
        }

        static string GetLink(GenericSyndicationFeed feed, GenericSyndicationItem item)
        {
            var link = "";
            if (feed.Format == SyndicationContentFormat.Rss)
            {
                RssFeed rssFeed = feed.Resource as RssFeed;
                if (rssFeed != null)
                {
                    var rssItem = rssFeed.Channel.Items.FirstOrDefault(i => i.Title == item.Title);
                    link = rssItem?.Link?.ToString();
                }
            }
            else if (feed.Format == SyndicationContentFormat.Atom)
            {
                AtomFeed atomFeed = feed.Resource as AtomFeed;
                if (atomFeed != null)
                {
                    var atomEntry = atomFeed.Entries.FirstOrDefault(e => e.Title.Content == item.Title);
                    link = atomEntry?.Links?.FirstOrDefault()?.Uri.ToString();
                }
            }

            return !string.IsNullOrWhiteSpace(link) ? link : "";
        }

        static async Task<string> GetFullStory(string url)
        {
            ReadSharp.Reader reader = new ReadSharp.Reader();
            ReadSharp.Article article;
            
            article = await reader.Read(new Uri(url));
            return article.Content;
        }

        static void AddHtmlHeader(StringBuilder sb)
        {
            sb.AppendLine("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            sb.AppendLine("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.1//EN\" \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">");
            sb.AppendLine("<html xmlns=\"http://www.w3.org/1999/xhtml\">");
            sb.AppendLine("<head>");
            sb.AppendLine("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />");
            sb.AppendLine("<title></title>");
            sb.AppendLine("</head>");
            sb.AppendLine("<body><div>");
        }

        static void AddHtmlFooter(StringBuilder sb)
        {
            sb.AppendLine("</div></body>");
            sb.AppendLine("</html>");
        }

        static string AddHtmlHeaderFooter(string str)
        {
            var sb = new StringBuilder();
            AddHtmlHeader(sb);
            sb.Append(str);
            AddHtmlFooter(sb);
            return sb.ToString();
        }

        static async Task<string> ToEpub(GenericSyndicationFeed feed)
        {
            var title = feed.Title ?? "";
            var author = GetAuthorFromFeed(feed);
            var dateString = DateTime.Now.ToString("dd_MM_yy_HH-mm-ss");
            var filename = EnsureValidFilename(title.Trim() + "_" + dateString + ".epub");
            using (var outFile = File.Create(filename))
            using (var writer = await EPubFactory.EPubWriter.CreateWriterAsync(outFile, title, author, Guid.NewGuid().ToString()))
            {
                int count = 1;
                var overviewBuilder = new StringBuilder();
                AddHtmlHeader(overviewBuilder);

                foreach (var item in feed.Items)
                {
                    var chapterFilename = (count++).ToString() + ".xhtml";
                    var itemDateString = "";
                    if (item.PublishedOn != null)
                    {
                        var d = item.PublishedOn;
                        itemDateString = "<small>" + d.ToShortDateString() + " " + d.ToShortTimeString() + "</small><br>";
                    }
                    var text =
                        "<p id=\"summary" + count.ToString() + "\">" +
                        "<b>" + item.Title + "</b><br>" +
                        itemDateString +
                        item.Summary + "<br>" +
                        "<a href=\"" + chapterFilename + "\">Artikel lesen</a>" +
                        "</p>";

                    overviewBuilder.AppendLine(text);    
                }

                AddHtmlFooter(overviewBuilder);
                await writer.AddChapterAsync("overview.xhtml", "Übersicht", overviewBuilder.ToString().Replace("<br>", "<br />"));

                var fullStories = await Task.WhenAll(feed.Items.Select(async item =>
                {
                    var linkText = GetLink(feed, item); // item.Links?.FirstOrDefault()?.Uri?.ToString();
                    var fullStory = "Konnte den Artikel nicht finden :(";
                    if (!string.IsNullOrWhiteSpace(linkText))
                    {
                        try
                        {
                            Console.WriteLine("Obtaining full article for " + linkText);
                            fullStory = await GetFullStory(linkText);
                        }
                        catch (Exception ex)
                        {
                            Console.Error.WriteLine("Could not get full story for " + linkText + "\n" + ex.GetMessage());
                            fullStory = linkText;
                        }
                    }
                    return fullStory;
                }));

                count = 1;
                int storyIdx = 0;
                foreach (var item in feed.Items)
                {
                    var overviewLink = "<a href=\"overview.xhtml#summary" + count.ToString() + "\">Zurück zur Übersicht</a><br />";
                    var chapterFilename = (count++).ToString() + ".xhtml";
                    var linkText = GetLink(feed, item); // item.Links?.FirstOrDefault()?.Uri?.ToString();
                    var fullStory = fullStories[storyIdx++];
                    var chapterTitle = item.Title ?? "Titel";
                    fullStory = "<h1>" + chapterTitle + "</h1><br />" + overviewLink + fullStory;
                    fullStory += overviewLink;
                    await writer.AddChapterAsync(chapterFilename, chapterTitle, AddHtmlHeaderFooter(fullStory).Replace("<br>", "<br />"));
                }

                await writer.WriteEndOfPackageAsync();
            }

            return filename;
        }

        static void SendMailForGeneratedFile(IEnumerable<string> filenames, Config config)
        {
            var mailConfig = config.Mail;
            MailMessage mail = new MailMessage(mailConfig.FromAddress, mailConfig.ToAddress, mailConfig.Subject, mailConfig.Body);
            SmtpClient client = new SmtpClient();
            client.Port = mailConfig.Port;
            client.EnableSsl = mailConfig.UseSsl;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.Credentials = new System.Net.NetworkCredential(mailConfig.Username, mailConfig.Password);
            client.Host = mailConfig.Server;
            foreach (var filename in filenames)
            {
                mail.Attachments.Add(new Attachment(filename));
            }
            client.Send(mail);
        }
    }
}
