﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSStoReader
{
    public class Config
    {
        public class MailConfig
        {
            public string Server;
            public int Port;
            public bool UseSsl;
            public string Username;
            public string Password;
            public string FromAddress;
            public string ToAddress;
            public string Subject;
            public string Body;
        }

        public string[] Feeds = new string[0];
        public MailConfig Mail = new MailConfig();
    }
}
